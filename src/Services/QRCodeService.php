<?php

namespace App\Services;

use Endroid\QrCode\Builder\BuilderInterface;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;

class QRCodeService
{
    protected $builder;

    public function __construct(BuilderInterface $builder)
    {
        $this->builder = $builder;    
    }

    public function QRCodeGenerator($query)
    {
        $url = 'https://www.tiktok.com/@';

        $result = $this->builder
            ->data($url.$query)
            ->encoding(new Encoding('UTF-8'))
            ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
            ->size(300)
            ->margin(10)
            ->labelText('@'.$query)
            ->build()
        ;

        //Génération du nom de QRCode
        $pngName = uniqid('','') . '.png';

        //Sauvegarde du QRCode dans le dossier
        $result->saveToFile((\dirname(__DIR__, 2) .'/public/assets/qrcode/'.$pngName));
        
        return $result->getDataUri();
    }
}