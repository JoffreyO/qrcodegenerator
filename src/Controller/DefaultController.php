<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\QrCodeFormType;
use App\Services\QRCodeService;

class DefaultController extends AbstractController
{

    #[Route('/', name: 'app_default')]
    public function index(Request $request, QRCodeService $qrCodeService): Response
    {

        $qrCode = null;
        $form = $this->createForm(QrCodeFormType::class, null);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $qrCode = $qrCodeService->QRCodeGenerator($data['search']);
        }


        return $this->render('public/index.html.twig', [
            'form' => $form->createView(),
            'qrCode' => $qrCode
        ]);
    }
}