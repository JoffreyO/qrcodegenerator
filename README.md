# QRCodeGenerator

Générateur de QR Code

Développé en Symfony 6 et PHP 8.1

## Getting started

Pour lancer le projet, veuillez à taper ces commandes
```
composer install
symfony server:start -d
```